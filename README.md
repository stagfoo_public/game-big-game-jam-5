# NAME_OF_GAME


## Graphics
[Mood Board](_pin.html.md)

### Models
- 2D

### Detail Level

- Paint

## Camera

- Sidescroller 

## Mechanics

this should be a brief outline of mechanics to include, they will morph and change during prototyping so dont worry

- [Single button leg walking](mechanic1.md)
- [Stacking](mechanic2.md)
- [upgrades](mechanic2.md)


# Systems

<!-- Are groups of game mechanics that work together. For example Pokemon and final fantasy have very similar battle systems they just look different graphically -->

- 

# Game Loop

explain how the mechanic work together

```mermaid

stateDiagram-v2
    MECHANIC1 --> MECHANIC2
    MECHANIC2 --> MECHANIC3
    MECHANIC3 --> MECHANIC4
    MECHANIC4 --> MECHANIC1
```

![funness](/.notes/funness.png)


# Level Design

generally generated after the camera, mechanic and framing are desideded


# Framing
<!-- This is often a story but not always. It’s the reason why the player is doing the gameplay loop. Puzzle games often has little to no framing. -->

- who is doing what in the story?
- why are they doing that?

